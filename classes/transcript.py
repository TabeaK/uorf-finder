#! /usr/bin/env/python

from uorf import Uorf
from codon import Codon
from global_functions import *

import logging
import copy

logger = logging.getLogger(__name__)


class Transcript:
    def __init__(self, info, fasta_genome_path, start_codons,
                 get_sequences_and_uorfs=True):
        """
        :param info:str the line from the ucsc table that contains info on the transcript
        """
        self.ucsc = copy.deepcopy(info)
        if not self.check_for_cds():
            logging.debug("This Transcript instance reports that the following transcript is invalid: %s" % info)
            return

        self.start_codons = start_codons
        self.fasta_genome_path = fasta_genome_path

        # convert str into int
        for c in ["txStart", "txEnd", "cdsStart", "cdsEnd", "exonCount", "score"]:
            try:
                self.ucsc[c] = int(self.ucsc[c])
            except ValueError:
                print("ERROR: The value %s=%s cannot be converted into integer for this transcript: %s." % (
                    c, self.ucsc[c], str(self)))
                import sys
                sys.exit(1)

        # convert "longblob" format into list
        self.ucsc['exonStarts'] = longblob2list(self.ucsc['exonStarts'])
        self.ucsc['exonEnds'] = longblob2list(self.ucsc['exonEnds'])
        self.ucsc['exonFrames'] = longblob2list(self.ucsc['exonFrames'])

        self.exon_lengths = self.get_exon_lengths()
        self.intron_lengths = self.get_intron_lengths()
        self.intron_nr = self.get_intron_nr()

        if self.ucsc['strand'] == "+":
            self.utr_five_start = self.ucsc['txStart']
            self.utr_five_end = self.ucsc['cdsStart']
        elif self.ucsc['strand'] == "-":
            self.utr_five_start = self.ucsc['cdsEnd']
            self.utr_five_end = self.ucsc['txEnd']
        else:
            raise Exception("There's no strand information given for the transcript: %s" % self.ucsc)
        self.intronStarts, self.intronEnds = self.get_intron_start_end()
        self.intron_coordinates = self.get_intron_coordinates()

        if self.ucsc['strand'] == "+":
            self.five_prime_utr_start, self.five_prime_utr_end = self.ucsc['txStart'], self.ucsc['cdsStart']
            self.three_prime_utr_start, self.three_prime_utr_end = self.ucsc['cdsEnd'], self.ucsc['txEnd']
        elif self.ucsc['strand'] == "-":
            self.five_prime_utr_start, self.five_prime_utr_end = self.ucsc['cdsEnd'], self.ucsc['txEnd']
            self.three_prime_utr_start, self.three_prime_utr_end = self.ucsc['txStart'], self.ucsc['cdsStart']

        self.cds_atg = self.get_cds_atg_pos()

        if get_sequences_and_uorfs:
            self.get_sequences()
            self.get_uorfs()

    def __str__(self):
        return "Transcript %s at %s:%s-%s (%s)" % (self.ucsc['name'], self.ucsc['chrom'], self.ucsc['txStart'],
                                                   self.ucsc['txEnd'], self.ucsc['strand'])

    def __repr__(self):
        return self.__str__()

    def get_sequences(self, just_full_transcript_seq=False):
        logging.debug("\n\n#~#~Get full transcript sequence \n")
        self.full_transcript_seq = get_masked_seq(start=self.ucsc['txStart'] + 1,
                                                  end=self.ucsc['txEnd'],
                                                  strand=self.ucsc['strand'],
                                                  intron_coordinates=self.intron_coordinates,
                                                  chrom=self.ucsc['chrom'], fasta_genome_path=self.fasta_genome_path)

        logger.debug("just_full_transcript_seq: %s" % just_full_transcript_seq)

        if just_full_transcript_seq:
            return

        logging.debug("\n\n#~#~ Get 5' UTRsequence \n")
        self.five_prime_utr_seq = get_masked_seq(start=self.five_prime_utr_start + 1,
                                                 end=self.five_prime_utr_end,
                                                 strand=self.ucsc['strand'],
                                                 intron_coordinates=self.intron_coordinates,
                                                 chrom=self.ucsc['chrom'],
                                                 fasta_genome_path=self.fasta_genome_path)
        logging.debug(self.five_prime_utr_seq)
        logging.debug("\n\n#~#~ Get CDS sequence \n")
        self.cds_seq = get_masked_seq(start=self.ucsc['cdsStart'] + 1, end=self.ucsc['cdsEnd'],
                                      strand=self.ucsc['strand'], intron_coordinates=self.intron_coordinates,
                                      chrom=self.ucsc['chrom'], fasta_genome_path=self.fasta_genome_path)
        logging.debug(self.cds_seq)

        logging.debug("\n\n#~#~ Get 3' UTR sequence \n")
        self.three_prime_utr_seq = get_masked_seq(start=self.three_prime_utr_start + 1,
                                                  end=self.three_prime_utr_end,
                                                  strand=self.ucsc['strand'],
                                                  intron_coordinates=self.intron_coordinates,
                                                  chrom=self.ucsc['chrom'],
                                                  fasta_genome_path=self.fasta_genome_path)
        logging.debug(self.three_prime_utr_seq)


        logging.debug("self.five_prime_utr_seq: %s" % (self.five_prime_utr_seq))
        logging.debug("self.full_transcript_seq: %s\npieced together: %s" % (self.full_transcript_seq,
                                                                             self.five_prime_utr_seq + self.cds_seq
                                                                             + self.three_prime_utr_seq))

    def get_uorfs(self):
        logging.debug("\n\n#~#~Get all start and stop codons for the 5'UTR, CDS and 3'UTR\n")

        logging.debug("\n\n#~#~Get all Start and Stop codons of UTR for all three frames\n")
        self.all_start_codons, self.all_stop_codons = get_start_stop_codons(
            get_codon_list(seq=self.full_transcript_seq,
                           seq_start=self.ucsc['txStart'],
                           seq_end=self.ucsc['txEnd'],
                           strand=self.ucsc['strand'],
                           start_codons=self.start_codons))
        self.five_prime_utr_start_codons, self.five_prime_utr_stop_codons = \
            get_start_stop_codons(get_codon_list(seq=self.five_prime_utr_seq,
                                                 seq_start=self.five_prime_utr_start,
                                                 seq_end=self.five_prime_utr_end,
                                                 strand=self.ucsc['strand'],
                                                 start_codons=self.start_codons))
        logging.debug("all Start codons: \n%s\n%s\n%s" % (self.all_start_codons[0], self.all_start_codons[1],
                                                          self.all_start_codons[2]))
        logging.debug("all Stop codons: \n%s\n%s\n%s" % (self.all_stop_codons[0], self.all_stop_codons[1],
                                                         self.all_stop_codons[2]))
        logging.debug("5' UTR Start codons: %s" % self.five_prime_utr_start_codons)
        logging.debug("5' UTR Stop codons: %s\n" % self.five_prime_utr_stop_codons)

        logging.debug("\n\n#~#~Get potential uORFs\n")
        self.uorf_list = self.get_uorf_list()

        logging.debug("GENE instance: %s" % vars(self))

    def check_for_cds(self):
        """
        Checks, whether this transcript has a CDS (is not a RNA transcript)
        :return:
        """
        # if the start coordinate and the end coordinate of the CDS are identical,
        # then there is no CDS...
        if self.ucsc['cdsStart'] == self.ucsc['cdsEnd']:
            return False
        return True

    def is_valid_transcript(self):
        """
        Checks, whether a transcript is valid, based on different criteria.
        :return:
        """
        if self.check_for_cds():
            return True
        return False

    def get_cds_atg_pos(self):
        """
        Returns the chromosome position (start and end) of the ATG of the CDS,
        i.e. the start codon.
        :return:
        """
        if self.ucsc['strand'] == "+":
            return [self.ucsc['cdsStart'], self.ucsc['cdsStart'] + 3]
        else:
            return [self.ucsc['cdsEnd'] - 3, self.ucsc['cdsEnd']]

    def get_uorfs(self):
        """

        :return:
        """
        return self.uorfs

    def get_uorf_number(self):
        """
        Gives the number of uORFs a transcript has
        :return: int
        """
        return len(self.uorfs)

    def print_uorfs(self):
        logging.debug("self.uorf_list: %s" % self.uorf_list)
        for i, UORF in enumerate(self.uorf_list):
            # logging.debug("uORF: ")
            print (i, UORF)

    def get_first_stop_for_start(self, start_codon, stop_codon_list):
        """
        Returns the first stop codon that is behind the start codon.
        """
        logger.debug("stop_codon_list: %s" % stop_codon_list)
        logger.debug("start_codon: %s" % start_codon)
        if self.ucsc['strand'] == '+':
            for stop_CODON in stop_codon_list:
                if stop_CODON.start >= start_codon.end:
                    return stop_CODON
        if self.ucsc['strand'] == '-':
            for stop_CODON in stop_codon_list:
                if stop_CODON.start <= start_codon.end:
                    return stop_CODON
        return None

    def get_matching_stop_for_start(self, f, start_codon):
        """
        Returns matching Stop codon and the number/index of the matching Stop codon.

        If there is no matching in-frame STOP in the 5' UTR, it takes the first STOP from the CDS.
        If there is also no in-frame STOP in the CDS, it takes the first STOP from the 3'UTR.
        If this doesn't contain a in-frame STOP either, then STOP is set to the transcription end of the transcript.

        :param f: int
        :param start_CODON: Codon instance
        :return: tuple of str with region_name and instance of class Codon
        """
        # logging.debug("Get matching stop for start: %s" % (start_codons,))
        logging.debug("Get matching stop for start. Stop list for frame %s: %s" % (f,
                                                                                   self.all_stop_codons[f]))
        stop_codon = self.get_first_stop_for_start(start_codon, self.all_stop_codons[f])
        if stop_codon == None:
            # There is no in-frame stop codon anywhere, therefore return the transcription end site
            # as the stop codon. This should happen only rarely...
            logging.debug("No in-frame stop codon anywhere!!")
            if self.ucsc['strand'] == "+":
                stop_codon = Codon("XXX", self.ucsc['txEnd'] - 2, self.ucsc['txEnd'] - 1, self.ucsc['txEnd'],
                                   start_codon)
            else:
                stop_codon = Codon("XXX", self.ucsc['txStart'] + 3, self.ucsc['txStart'] + 2, self.ucsc['txStart'] + 1,
                                   start_codon)
            logging.debug("Stop codon: %s" % stop_codon)
            return "transcription_end", stop_codon
        region_name = self.get_name_of_region_codon_is_located_in(stop_codon)
        logging.debug("Stop codon: %s" % stop_codon)
        return region_name, stop_codon

    def get_name_of_region_codon_is_located_in(self, codon):
        """
        Returns the name of the region a codon is located in, i.e. either 5'UTR, CDS or 3'UTR!
        """
        logging.debug("get_name_of_region_codon_is_located_in")
        logging.debug("codon: %s" % codon)
        if codon == None:
            return codon
        if self.ucsc['strand'] == '+':
            # if the entire codon is located within 5'UTR
            if codon.biggest < self.ucsc['cdsStart']:
                return "5'UTR"
            # if the beginning of the codon is located within 5'UTR and the end is within the CDS
            elif codon.smallest < self.ucsc['cdsStart'] >= codon.mid and codon.biggest >= self.ucsc['cdsStart']:
                return "5'UTR_CDS_overlap"
            elif codon.biggest == self.ucsc['cdsEnd']:
                return "CDS_end"
            # if the beginning of the codon is located within the CDS and is before the 3'UTR
            elif self.ucsc['cdsStart'] <= codon.start >= self.ucsc['cdsStart'] and \
                 self.ucsc['cdsStart'] <= codon.mid   >= self.ucsc['cdsStart'] and \
                 self.ucsc['cdsStart'] <= codon.end   >= self.ucsc['cdsStart']:
                return "within_CDS"
            # if codon is located within the 3'UTR
            elif codon.smallest >= self.ucsc['cdsEnd']:
                return "3'UTR"
            else:
                logging.error(
                    "Cannot determine in which region of the transcript %s this codon is located: %s" % (self, codon))
        if self.ucsc['strand'] == '-':
            # logging.debug("%s <= %s %s %s >= %s" % (self.ucsc['cdsStart'], codon.start, codon.mid, codon.end, self.ucsc['cdsEnd']))
            # logging.debug("self.ucsc['cdsStart'] <= codon.start <= self.ucsc['cdsEnd']: %s" % (self.ucsc['cdsStart'] <= codon.start <= self.ucsc['cdsEnd']))
            # logging.debug("self.ucsc['cdsStart'] <= codon.mid   <= self.ucsc['cdsEnd']: %s" % (self.ucsc['cdsStart'] <= codon.mid   <= self.ucsc['cdsEnd']))
            # logging.debug("self.ucsc['cdsStart'] <= codon.end   <= self.ucsc['cdsEnd']: %s" % (self.ucsc['cdsStart'] <= codon.end   <= self.ucsc['cdsEnd']))
            # if the entire codon is located within 5'UTR
            if codon.smallest > self.ucsc['cdsEnd']:
                return "5'UTR"
            # if the beginning of the codon is located within 5'UTR and the end is within the CDS
            elif codon.smallest < self.ucsc['cdsEnd'] and codon.biggest >= self.ucsc['cdsEnd']:
                return "5'UTR_CDS_overlap"
            # if the beginning of the codon is located within the CDS and is before the 3'UTR
            elif self.ucsc['cdsStart'] <= codon.start <= self.ucsc['cdsEnd'] and \
                 self.ucsc['cdsStart'] <= codon.mid   <= self.ucsc['cdsEnd'] and \
                 self.ucsc['cdsStart'] <= codon.end   <= self.ucsc['cdsEnd']:
                return "CDS"
            # if codon is located within the 3'UTR
            elif self.ucsc['txStart'] <= codon.start >= self.ucsc['cdsStart'] and \
                 self.ucsc['txStart'] <= codon.mid   >= self.ucsc['cdsStart'] and \
                 self.ucsc['txStart'] <= codon.end   >= self.ucsc['cdsStart']:
                return "3'UTR"
            else:
                logging.error(
                    "Cannot determine in which region of the transcript %s this codon is located: %s" % (self, codon))
        return "None"

    def get_uorf_list(self):
        """
        Returns the coordinates of all potential uorfs, based on the criteria, that they
        need to have a start codon before a stop codon (obviously)
        The very first stop codon following the start codon will be used.
        If there is no stop codon known that follows the start codon, this
        is reported as an "-" end.
        """
        UORF_list = []
        # iterate over each of the three frames
        for f in [0, 1, 2]:
            logging.debug("get_uorf_list frame: %s" % f)
            start_codon_list = self.five_prime_utr_start_codons[f]

            # iterate over each start codon for this frame
            for start_codon_nr, start_codon in enumerate(start_codon_list):
                logging.debug("create UORF object for start_codons: %s" % start_codon)

                stop_located_in, stop_codon = self.get_matching_stop_for_start(f, start_codon)

                UORF = Uorf(start_codon=start_codon,
                            stop_codon=stop_codon,
                            frame=f,
                            transcript=self,
                            stop_located_in=stop_located_in)
                UORF_list.append(UORF)
                logging.debug("UORF: %s" % vars(UORF))
        return UORF_list

    def get_intron_coordinates(self):
        """
        Returns a list of coordinates in this style:
        [['chr1', 123, 456], ['chr1', 567, 789]]
        :return: list
        """
        regions = []
        for intron_nr, intronstart in enumerate(self.intronStarts):
            intronend = self.intronEnds[intron_nr]
            regions.append([self.ucsc['chrom'],
                            intronstart,
                            intronend])
        logging.debug("Intron coordinates: %s" % regions)
        return regions

    def get_exonstarts_end(self):
        """
        Generator giving the starts and ends of all exons!
        :return:
        """
        for i in range(0, len(self.ucsc['exonStarts'])):
            yield self.ucsc['exonStarts'][i], self.ucsc['exonEnds'][i]

    def get_intronstarts_end(self):
        """
        Generator giving the starts and ends of all exons!
        :return:
        """
        for i in range(0, len(self.intronStarts)):
            yield self.intronStarts[i], self.intronEnds[i]

    def get_intron_start_end(self):
        """
        Sets the genomic intron start and end coordinates!
        :return:
        """
        intronStarts = self.ucsc['exonEnds'][:-1]
        intronEnds = self.ucsc['exonStarts'][1:]
        if self.ucsc['strand'] == '-':
            intronStarts = intronStarts[::-1]
            intronEnds = intronEnds[::-1]
        return intronStarts, intronEnds

    def get_exon_lengths(self):
        """
        Returns a list of exon lengths
        """
        exon_lengths = []
        for exon_nr in range(0, len(self.ucsc['exonStarts'])):
            exon_lengths.append(self.ucsc['exonEnds'][exon_nr] - self.ucsc['exonStarts'][exon_nr])
        return exon_lengths

    def get_intron_lengths(self):
        """
        Returns a list of intron lengths
        """
        intron_lengths = []
        for exon_nr in range(1, len(self.ucsc['exonStarts'])):
            if self.ucsc['strand'] == "+":
                intron_lengths.append(self.ucsc['exonStarts'][exon_nr] - self.ucsc['exonEnds'][exon_nr - 1])
            elif self.ucsc['strand'] == "-":
                intron_lengths.append(self.ucsc['exonStarts'][exon_nr - 1] - self.ucsc['exonEnds'][exon_nr])
        return intron_lengths

    def get_intron_nr(self):
        return len(self.intron_lengths)
