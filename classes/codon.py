#! /usr/bin/env/python

import logging
from global_functions import *

logger = logging.getLogger(__name__)


class Codon():
    stop_codons = ['TAA', 'TAG', 'TGA']
    start_codons = []

    def __init__(self, codon_seq, codon_start_pos, codon_mid_pos, codon_end_pos, start_codons):
        self.start_codons = start_codons
        self.seq = codon_seq
        self.start = codon_start_pos
        self.mid = codon_mid_pos
        self.end = codon_end_pos
        self.smallest = min(codon_start_pos, codon_mid_pos, codon_end_pos)
        self.biggest = max(codon_start_pos, codon_mid_pos, codon_end_pos)
        # self.sort_positions()

    def __str__(self):
        return "%s: %s, %s, %s" % (self.seq, self.start, self.mid, self.end)

    def __repr__(self):
        return self.__str__()

    def check(self):
        """
        Prints a message, if positions are not consecutive.
        This is just for debugging, to inform about codons stretching across introns.
        """
        print(self.start == self.mid - 1)
        print(self.start == self.end - 2)
        if not (self.start == self.mid - 1) and (self.start == self.end - 2):
            logging.debug("The positions given for the codon are not consecutive: %s" % str(self))

    def is_start_codon(self):
        """
        Returns true, if the codon is a Start codon.
        :return:
        """
        if self.seq in self.start_codons:
            return True
        return False

    def is_stop_codon(self):
        """
        Returns true, if the codon is a Stop codon.
        :return:
        """
        if self.seq in self.stop_codons:
            return True
        return False
