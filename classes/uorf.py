#! /usr/bin/env/python

import logging
from global_functions import *
from Bio.Seq import Seq
import sys


logger = logging.getLogger(__name__)


class ErrorUorfLengthNotDivisibleBy3(Exception):
    def __str__(self):
        return repr(self)

class Uorf(object):
    def __init__(self, **kwargs):
        """
        When invoked, sets self.transcript to TRANSCRIPT instance.
        """
        self.__dict__.update(kwargs)

        logging.debug("#~# Create uORF object!")
        logging.debug("#~#~#~Get uORF sequence")
        self.seq = self.get_seq()
        # self.seq = get_dna_seq(self.transcript.ucsc['chrom'], self.start_codons.start, self.stop_codons.end, self.transcript.fasta_genome_path)
        self.seq_no_introns = get_seq_without_introns(self.seq)
        if len(self.seq_no_introns) % 3 != 0 and not self.stop_located_in == "transcription_end":
            logging.error("ERROR: The length of the uorf sequence (%s) is not divisible by 3!! \n%s" %
                          (len(self.seq_no_introns), self.seq_no_introns))
            sys.exit(1)
        else:
            logging.debug("Length of sequence (%s) is divisible by 3" % len(self.seq_no_introns))
        self.length = len(self.seq)
        self.length_no_introns = len(self.seq_no_introns)
        self.length_of_introns = get_length_of_introns(self.seq)
        self.uorf_start_position_in_mrna = self.get_uorf_start_position_in_mrna()
        self.uorf_end_position_in_mrna = self.get_uorf_end_position_in_mrna()
        self.distance_to_cds_start = self.get_distance_to_cds_start()
        logging.debug(" self.distance_to_cds_start: %s" % self.distance_to_cds_start)
        self.distance_to_cds_start_no_introns = self.get_distance_to_cds_start_no_introns()

        logging.debug("^^: %s" % vars(self))
        logging.debug("Start codon: %s" % str(self.start_codon))
        logging.debug("Stop  codon: %s" % str(self.stop_codon))

        logging.debug("#~#~#~Get kozak sequence\n")
        self.kozak = self.get_kozak_sequence()

        logging.debug("\n\n#~#~#~Get coordinates of uORF exons\n")
        self.exon_block_coordinates = self.get_block_coordinates()

    def __str__(self):
        return "uORF of transcript %s (%s) on frame %s > %s:%s-%s of length %s with seq: %s" % (
            self.transcript.ucsc['name'], self.transcript.ucsc['strand'], self.frame, self.transcript.ucsc['chrom'],
            self.start_codon.start,
            self.stop_codon.end,
            self.length_no_introns,
            self.seq_no_introns)

    def print_fasta_sequence(self):
        print (">%s:%s-%s\n%s" % (
            self.transcript.ucsc['chrom'], self.start_codon.start, self.stop_codon.end, self.seq_no_introns))

    def summary(self, uorf_count):
        """
        Returns the line that is to be printed into the
        output file.
        """
        if self.transcript.ucsc['strand'] == '+':
            start = self.start_codon.start
            stop = self.stop_codon.end
        elif self.transcript.ucsc['strand'] == '-':
            start = self.stop_codon.end
            stop = self.start_codon.start
            # logging.debug("Start codon is %s and Stop codon is %s" % (start_codon_seq, stop_codon_seq))
        s = "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (
            self.transcript.ucsc['chrom'], start, stop, self.transcript.ucsc['name'], uorf_count,
            self.transcript.ucsc['strand'], self.length,
            self.length_no_introns,
            self.distance_to_cds_start,
            self.distance_to_cds_start_no_introns,
            self.uorf_start_position_in_mrna,
            self.uorf_end_position_in_mrna,
            self.frame,
            self.kozak,
            self.start_codon.seq,
            self.stop_codon.seq,
            # self.stretches_into_cds,
            # self.stretches_into_three_prime_utr,
            # self.no_in_frame_stop,
            self.stop_located_in,
            self.seq_no_introns,
            self.seq,
            self.transcript.ucsc['name2'])
        logging.debug("%s" % s)
        logging.debug("%s >= %s --> %s" % (start, stop, start >= stop))
        if(start >= stop):
            logging.critical("The coordinates in the summary line is incorrect, because the "
                     "start coordinate is larger than the end coordinate: \n%s" % s)
            sys.exit(1)
        return(s)

    def get_start_stop_bed(self, uorf_count):
        line_start1 = "\t".join(map(str, [self.transcript.ucsc['chrom'],
                                          self.start_codon.start - 1, self.start_codon.start,
                                          "%s_%s_start_%s_%s" % (
                                          self.transcript.ucsc['name'], uorf_count, self.start_codon.seq,
                                          self.stop_located_in),
                                          self.frame, self.transcript.ucsc['strand']]))
        line_start2 = "\t".join(map(str, [self.transcript.ucsc['chrom'],
                                          self.start_codon.mid - 1, self.start_codon.mid,
                                          "%s_%s_start_%s_%s" % (
                                          self.transcript.ucsc['name'], uorf_count, self.start_codon.seq,
                                          self.stop_located_in),
                                          self.frame, self.transcript.ucsc['strand']]))
        line_start3 = "\t".join(map(str, [self.transcript.ucsc['chrom'],
                                          self.start_codon.end - 1, self.start_codon.end,
                                          "%s_%s_start_%s_%s" % (
                                          self.transcript.ucsc['name'], uorf_count, self.start_codon.seq,
                                          self.stop_located_in),
                                          self.frame, self.transcript.ucsc['strand']]))
        line_stop1 = "\t".join(map(str, [self.transcript.ucsc['chrom'],
                                         self.stop_codon.start - 1, self.stop_codon.start,
                                         "%s_%s_stop_%s_%s" % (
                                         self.transcript.ucsc['name'], uorf_count, self.start_codon.seq,
                                         self.stop_located_in),
                                         self.frame, self.transcript.ucsc['strand']]))
        line_stop2 = "\t".join(map(str, [self.transcript.ucsc['chrom'],
                                         self.stop_codon.mid - 1, self.stop_codon.mid,
                                         "%s_%s_stop_%s_%s" % (
                                         self.transcript.ucsc['name'], uorf_count, self.start_codon.seq,
                                         self.stop_located_in),
                                         self.frame, self.transcript.ucsc['strand']]))
        line_stop3 = "\t".join(map(str, [self.transcript.ucsc['chrom'],
                                         self.stop_codon.end - 1, self.stop_codon.end,
                                         "%s_%s_stop_%s_%s" % (
                                         self.transcript.ucsc['name'], uorf_count, self.start_codon.seq,
                                         self.stop_located_in),
                                         self.frame, self.transcript.ucsc['strand']]))
        line = "\n".join([line_start1, line_start2, line_start3, line_stop1, line_stop2, line_stop3]) + "\n"
        logging.debug("\n%s" % line)
        return line

    def get_distance_to_cds_start(self):
        """
        Gives the distance between the uORF end position and the CDS start,
        including introns.
        :return:
        """
        # logging.debug("Get distance of uORF end to CDS start!")
        if self.transcript.ucsc['strand'] == '-':
            if not self.stop_located_in == "3'UTR":
                return self.transcript.ucsc['cdsEnd'] - self.start_codon.start
            else:
                return self.stop_codon.end - self.transcript.ucsc['cdsEnd']
        else:
            return abs(self.stop_codon.end - self.transcript.ucsc['cdsStart'])

    def get_distance_to_cds_start_no_introns(self):
        """
        Gives the distance between the uORF end position and the CDS start,
        excluding introns.
        :return:
        """
        if self.transcript.ucsc['strand'] == '-':
            if not self.stop_located_in == "3'UTR":
                intron_length = get_intron_nucleotide_count_between_two_positions(self.start_codon.start,
                                                                                  self.transcript.ucsc['cdsEnd'],
                                                                                  self.transcript.intron_coordinates)
            else:
                intron_length = get_intron_nucleotide_count_between_two_positions(self.stop_codon.end,
                                                                                  self.transcript.ucsc['cdsEnd'],
                                                                                  self.transcript.intron_coordinates)
        else:
            intron_length = get_intron_nucleotide_count_between_two_positions(self.stop_codon.end,
                                                                              self.transcript.ucsc['cdsStart'],
                                                                              self.transcript.intron_coordinates)
        logging.debug("Intron length is %s" % intron_length)
        return abs(self.distance_to_cds_start) - intron_length

    def get_uorf_start_position_in_mrna(self):
        return get_seq_without_introns(self.transcript.full_transcript_seq).find(self.seq_no_introns) + 1

    def get_uorf_end_position_in_mrna(self):
        return get_seq_without_introns(self.transcript.full_transcript_seq).find(self.seq_no_introns) + len(
            self.seq_no_introns)

    def get_seq(self):
        """
        Get the uORF sequence.
        All intronic regions (i.e. intronic for this particular transcript) are masked with lowercase letters!
        """
        logging.debug("$$$: Get uORF sequence: %s" % (vars(self)))
        if self.transcript.ucsc["strand"] == "+":
            start = self.start_codon.smallest
            stop = self.stop_codon.biggest
        if self.transcript.ucsc["strand"] == "-":
            start = self.stop_codon.smallest
            stop = self.start_codon.biggest
        logging.debug("$$$: Get uORF sequence: start: %s stop: %s" % (start, stop))
        seq = get_masked_seq(chrom=self.transcript.ucsc['chrom'],
                             start=start,
                             end=stop,
                             fasta_genome_path=self.transcript.fasta_genome_path, strand=self.transcript.ucsc['strand'],
                             intron_coordinates=self.transcript.intron_coordinates)
        logging.debug("Uorf seq (%s-%s): %s" % (start, stop, seq))
        return seq

    def get_kozak_sequence(self):
        """
        Returns 6 nucleotides ahead and 3 nucleotides behind
        a given position.
        e.g.: ccrcccAtgg
        """
        pos = self.start_codon.start
        logging.debug("kozak position: %s" % pos)
        logging.debug("Getting sequence ahead of start codon")
        if self.transcript.ucsc['strand'] == "+":
            rel_pos = pos - self.transcript.ucsc['txStart']
        else:
            rel_pos = pos - self.transcript.ucsc['txEnd']
            pos = pos - 1
        logging.debug("rel_pos: %s" % rel_pos)
        logging.debug("len(self.transcript.full_transcript_seq): %s" % len(self.transcript.full_transcript_seq))

        if rel_pos > 7:
            seq = self.transcript.full_transcript_seq[rel_pos - 7:rel_pos + 3]
        else:
            if self.transcript.ucsc['strand'] == "+":
                seq = get_exon_nucleotides_ahead_of_pos(pos, 7, self.transcript.intron_coordinates,
                                                        self.transcript.ucsc['chrom'],
                                                        self.transcript.fasta_genome_path)
                seq += get_exon_nucleotides_behind_pos(pos, 3, self.transcript.intron_coordinates,
                                                       self.transcript.ucsc['chrom'], self.transcript.fasta_genome_path)
            else:
                seq = get_exon_nucleotides_ahead_of_pos(pos, 3, self.transcript.intron_coordinates,
                                                        self.transcript.ucsc['chrom'],
                                                        self.transcript.fasta_genome_path)
                seq += get_exon_nucleotides_behind_pos(pos, 7, self.transcript.intron_coordinates,
                                                       self.transcript.ucsc['chrom'], self.transcript.fasta_genome_path)

        logging.debug("Getting the sequence surrounding position %s, i.e. the kozak sequence!" % pos)
        if self.transcript.ucsc['strand'] == '-':
            SEQ = Seq(seq)
            SEQ = SEQ.reverse_complement()
            seq = str(SEQ)
        logging.debug("Kozak sequence: %s" % seq)
        return seq

    def get_block_coordinates(self, include_intron_coordinates=False):
        """
        Returns the bed block coordinates for the exons in the uORF.
        :return:
        """
        # logging.debug("get_block_coordinates for %s" % self.seq)
        import re
        # split into intron and exon sequence
        starts = []
        stops = []
        x = 0
        intron_exon_blocks = filter(None, re.split("([ACTG][^actg]*)", self.seq))
        logging.debug("intron_exon_blocks: %s" % intron_exon_blocks)
        # correct for minus strand
        if self.transcript.ucsc['strand'] == '-':
            x = -1
            intron_exon_blocks = intron_exon_blocks[::-1]
            logging.debug("intron_exon_blocks: %s" % intron_exon_blocks)
        # split the sequence into blocks of introns and exons, based on the case
        for i, intron_exon_block in enumerate(intron_exon_blocks):
            logging.debug("i: %s block: %s" % (i, intron_exon_block))
            if str.istitle(intron_exon_block[0]):
                # is upper case -> exon!
                starts.append(self.start_codon.start + x)
                stops.append(self.start_codon.start + x + len(intron_exon_block))
            else:
                # is intron!
                if include_intron_coordinates:
                    starts.append(self.start_codon.start + x)
                    stops.append(self.start_codon.start + x + len(intron_exon_block))
                else:
                    pass
            x += len(intron_exon_block)
        chrom = [self.transcript.ucsc['chrom']] * len(starts)
        coordinates = zip(chrom, starts, stops)

        s = "\n"
        for coord in coordinates:
            s += "%s\t%s\t%s\n" % coord
        logging.debug("%s" % s)
        return coordinates

    def get_bed_twelve_line(self, uorf_count="nA"):
        """
        Returns the BED12 style line for this uORF!
        There is one "block" for each uORF exon.
        """
        block_coordinates = self.get_block_coordinates()
        block_starts, block_sizes = [], []
        if self.transcript.ucsc['strand'] == "+":
            for (chrom, block_start, block_end) in block_coordinates:
                block_sizes.append(block_end - block_start)
                block_starts.append(block_start - self.start_codon.start)
            bed_line = [self.transcript.ucsc['chrom'],
                        min(self.start_codon.start - 1, self.stop_codon.end),
                        max(self.start_codon.start - 1, self.stop_codon.end),
                        self.transcript.ucsc['name'] + "_" + str(uorf_count) + "_" + self.start_codon.seq,
                        0,
                        self.transcript.ucsc['strand'],
                        min(self.start_codon.start - 1, self.stop_codon.end),
                        max(self.start_codon.start - 1, self.stop_codon.end),
                        '255,165,200',
                        len(block_coordinates),
                        ",".join(map(str, block_sizes)),
                        ",".join(map(str, block_starts))]
        else:
            for (chrom, block_start, block_end) in block_coordinates:
                block_sizes.append(block_end - block_start)
                block_starts.append(max(0, block_start - self.start_codon.start + 1))
            bed_line = [self.transcript.ucsc['chrom'],
                        min(self.start_codon.start - 1, self.stop_codon.end - 1),
                        max(self.start_codon.start, self.stop_codon.end),
                        self.transcript.ucsc['name'] + "_" + str(uorf_count) + "_" + self.start_codon.seq,
                        0,
                        self.transcript.ucsc['strand'],
                        min(self.start_codon.start - 1, self.stop_codon.end - 1),
                        max(self.start_codon.start, self.stop_codon.end),
                        '255,165,200',
                        len(block_coordinates),
                        ",".join(map(str, block_sizes)),
                        ",".join(map(str, block_starts))]
        logging.debug(bed_line)
        if bed_line[1] >= bed_line[2]:
            logging.critical("The coordinates in the BED line is incorrect, because the "
                             "start coordinate is larger than the end coordinate: \n%s" % bed_line)
            sys.exit(1)
        return "\t".join(map(str, bed_line))



