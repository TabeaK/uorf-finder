# Readme for uORF-finder

## How to use the uORF-finder

```
usage: uorf_finder.py [-h] -a {hg18,hg19,hg38} -f GENOME_FASTA_FILE -g
                      {refGene,ncbiRefSeq} [-t TEST_MODE] -c
                      {non-canonical,ATG} [-o OUTPUT_FILE]
                      [-b OUTPUT_BED_FILE] [-s OUTPUT_START_STOP_FILE] [-d]
                      [-v]

optional arguments:
  -h, --help            show this help message and exit
  -a {hg18,hg19,hg38}, --assembly {hg18,hg19,hg38}
                        the assembly to use
  -f GENOME_FASTA_FILE, --genome_fasta_file GENOME_FASTA_FILE
                        the fasta file with the genome to use
  -g {refGene,ncbiRefSeq}, --geneset {refGene,ncbiRefSeq}
                        the geneset to use
  -t TEST_MODE, --test_mode TEST_MODE
                        Run in test mode and use the specified RefSeq file
  -c {non-canonical,ATG}, --start_codon_choice {non-canonical,ATG}
                        Choose between "non-canonical" and "ATG". Non-
                        canonical start codons are ['ATG', 'TTG', 'GTG',
                        'CTG', 'AAG', 'AGG', 'ACG', 'ATA', 'ATT', 'ATC'].
  -o OUTPUT_FILE, --output_file OUTPUT_FILE
                        Output file
  -b OUTPUT_BED_FILE, --output_bed_file OUTPUT_BED_FILE
                        Output BED file in which to write block coordinates
  -s OUTPUT_START_STOP_FILE, --output_start_stop_file OUTPUT_START_STOP_FILE
                        Output BED file in which to write only coordinates of
                        the uORF START and STOP codons.
  -d, --debug           Print debug messages
  -v, --verbose         Print verbose messages

```
