#!/usr/bin/python

"""
This script can be used to "combine" several uORFs that occur at the same positions, but are
assigned to different genes.
I.e., sometimes there is the same uORF coordinate, but it is found multiple times in multiple
genes.
"""

import sys
from collections import defaultdict

inputfile_fh = open(sys.argv[1], 'r')


d = defaultdict(list)

for i, line in enumerate(inputfile_fh.readlines()):
    line_split = line.strip().split("\t")
    coord = tuple(line_split[0:3] + [line_split[5]])
    # print coord
    d[coord].append(line_split)
    # print d[coord]


for i, coord in enumerate(d):
    # print i, coord, d[coord]
    lines_split = d[coord]
    # print len(lines_split)
    if len(lines_split) == 1:
        # simply print the line
        # print line_split
        # print "just 1, print it"
        new_line = '\t'.join(line_split[0:3] + ["%s#%s" % (line_split[3], line_split[4])] + [line_split[4]]
                             + [line_split[5]])
    else:
        # print a combined version of all lines
        # print "more than 1, combine them!"
        new_line = '\t'.join(coord[0:3])
        names = ''
        frame = 9
        names_unique = []
        for each_line in d[coord]:
            # print "each_line: %s" % each_line
            names += '%s#%s@' % (each_line[3], each_line[4])
        names = names.rstrip('@')
        new_line += '\t%s\t9\t%s' % (names, each_line[5])
    print new_line, "\t", len(d[coord])
