#! /usr/bin/env/python
"""
This pipeline finds positions of uORFs for all transcripts in a given dataset.

Author:  Tabea Kischka
Licensed under the MIT License.
"""

import sys
from tempfile import NamedTemporaryFile
import argparse
import logging
import time
from csv import DictReader
from classes.transcript import Transcript
from global_functions import *

min_uORF_len = 0
goldenpath_URL = "http://hgdownload.cse.ucsc.edu/goldenpath/ASSEMBLY/database/GENESET.txt.gz"

ucsc_table_fieldnames = ["bin", "name", "chrom", "strand",
                         "txStart", "txEnd", "cdsStart", "cdsEnd",
                         "exonCount", "exonStarts",
                         "exonEnds", "score", "name2",
                         "cdsStartStat", "cdsEndStat", "exonFrames"]
output_file_header = '\t'.join(["chrom", "start", "stop", "id", "uorf_count", "strand", "uorf_length_incl_introns",
                                "uorf_length_excl_introns", "cds_start_dist", "cds_start_dist_no_introns",
                                "start_pos_mRNA", "end_pos_mRNA", "frame", "kozak_context",
                                "start_codon", "stop_codon", "stop_located_in",
                                "seq_no_introns", "seq", "gene_symbol"])

non_canonical_start_codons = ["ATG", "TTG", "GTG", "CTG", "AAG", "AGG", "ACG", "ATA", "ATT", "ATC"]


def extract_gunzipfile(gz_filename, ungz_filename):
    """
    Extracts a given gunzip file.
    :param gz_filename: Path to the file that is in .gz format
    :return: ungz_filename: path to the filename that contains the extracted data
    """
    import gzip
    logging.debug("Extract gzipped file %s into %s" % (gz_filename, ungz_filename))
    if not os.path.isfile(gz_filename):
        sys.stderr("Error: The file %s does not exist. "
                   "There was a problem with downloading the table from UCSC." % gz_filename)
        sys.exit(1)
    gz = gzip.open(gz_filename)
    with open(ungz_filename, 'w') as fh:
        fh.writelines(gz.read())
    return ungz_filename


def sort_ucsc_table(tablefile):
    """
    Sorts the UCSC table
    """
    import subprocess
    cmd = "sort -k3V -k5n -k6n %s -o %s" % (tablefile, tablefile)
    logging.debug("Sorting UCSC table with this command: %s" % cmd)
    subprocess.call(cmd.split())


def get_ucsc_table(assembly, geneset):
    """
    Downloads a table from UCSC via mysql
    :param assembly: the assembly, specified by the user
    :param geneset: the geneset, specified by the user
    :return: the name of a file containing the downloaded table
    """
    from urllib import urlretrieve
    out_file = NamedTemporaryFile(delete=False, suffix='.gz')
    out_filename = out_file.name
    url = goldenpath_URL.replace('ASSEMBLY', assembly).replace('GENESET', geneset)
    logging.debug("Downloading %s to %s" % (url, out_filename))
    a, b = urlretrieve(url, out_filename)
    logging.debug("Done with download.")
    out_file.close()
    return out_filename


def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('-a', '--assembly',
                        help='the assembly to use (e.g. hg19, mm39, ...)',
                        type=str,
                        required=True
                        )
    parser.add_argument('-f', '--genome_fasta_file',
                        help='the fasta file with the genome to use',
                        type=str,
                        required=True
                        )
    parser.add_argument('-g', '--geneset',
                        help='the geneset to use',
                        type=str,
                        choices=['refGene', 'ncbiRefSeq'],
                        required=True
                        )
    parser.add_argument('-t', '--test_mode',
                        help='Run in test mode and use the specified RefSeq file',
                        type=str,
                        default=False
                        )
    parser.add_argument('-c', '--start_codon_choice',
                        help='Choose between "non-canonical" and "ATG". Non-canonical start codons are %s.' % non_canonical_start_codons,
                        type=str,
                        choices=['non-canonical', 'ATG'],
                        required=True
                        )
    parser.add_argument('-o', '--output_file',
                        help='Output file',
                        type=argparse.FileType('a'),
                        default=sys.stdout)
    parser.add_argument('-b', '--output_bed_file',
                        help='Output BED file in which to write block coordinates',
                        type=argparse.FileType('a'),
                        default=sys.stdout)
    parser.add_argument('-s', '--output_start_stop_file',
                        help='Output BED file in which to write only coordinates of the uORF START and STOP codons.',
                        type=argparse.FileType('a'),
                        default=sys.stdout)
    parser.add_argument('-d', '--debug',
                        help='Print debug messages',
                        action='store_const',
                        dest='loglevel',
                        const=logging.DEBUG,
                        default=logging.WARNING)
    parser.add_argument('-v', '--verbose',
                        help='Print verbose messages',
                        action='store_const',
                        dest='loglevel',
                        const=logging.INFO)
    args = parser.parse_args()

    # init logging config for debugging, etc
    logging.basicConfig(format='[%(asctime)s - %(levelname)s] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level=args.loglevel)
    logging.info("Start!")
    logging.info(args)

    fasta_genome_path = args.genome_fasta_file

    if args.start_codon_choice == "ATG":
        logger.info("The following start codon will be used: ATG")
        start_codons = ["ATG"]
    elif args.start_codon_choice == "non-canonical":
        logger.info("The following start codons will be used: %s" % non_canonical_start_codons)
        start_codons = non_canonical_start_codons

    # download transcript file and extract it (it's compressed)
    if args.test_mode:
        ucsc_table = args.test_mode
    else:
        if not os.path.exists('data'):
            os.makedirs('data')
        ucsc_table = "data/%s_%s.txt" % (args.assembly, args.geneset)
        ucsc_table_gz = get_ucsc_table(assembly=args.assembly, geneset=args.geneset)  # , test_mode=args.test_mode)
        extract_gunzipfile(ucsc_table_gz, ucsc_table)
        sort_ucsc_table(ucsc_table)

    logging.info("Downloaded transcript info file: %s" % ucsc_table)
    total_transcript_nr = get_line_count_for_file(ucsc_table)
    uorf_count = 0
    transcript_nr, invalid_transcript_nr = 0, 0

    args.output_file.writelines(output_file_header + "\n")

    with open(ucsc_table) as csvfile:
        reader = DictReader(csvfile, fieldnames=ucsc_table_fieldnames, dialect='excel-tab')
        logging.debug("Reading in csv file: %s" % vars(reader))
        start_time = int(round(time.time() * 1000)) # start time in milliseconds
        h, m, s, time_per_row = 0, 0, 0, 1000
        for row_nr, row in enumerate(reader):
            time_to_go = "Estimated time remaining: %02d:%02d:%02d, msec per transcript: %04d" % (h, m, s, time_per_row)
            # get a Transcript instance for the current transcript
            logging.info("[%06d/%06d]\t[%s]\tuORF count: %06d\tCurrent transcript: %s (%s)" % (
            transcript_nr, total_transcript_nr,
                                                                                        time_to_go,
                                                                                        uorf_count, row['name'],
                                                                                        row['strand']))
            TRANSCRIPT = Transcript(row, fasta_genome_path=fasta_genome_path, start_codons=start_codons)
            transcript_nr += 1

            # skip invalid transcripts!
            if not TRANSCRIPT.check_for_cds():
                logging.info("[%06d/%06d]\t[%s]\tuORF count: %06d\tTranscript without CDS: %s (%s)" % (
                transcript_nr, total_transcript_nr,
                                                                                                time_to_go,
                                                                                                uorf_count, row['name'],
                                                                                                row['strand']))
                invalid_transcript_nr += 1
                continue

            for UORF in TRANSCRIPT.uorf_list:
                uorf_count += 1
                args.output_file.write(UORF.summary(uorf_count) + '\n')
                args.output_bed_file.write(UORF.get_bed_twelve_line(uorf_count) + "\n")
                args.output_start_stop_file.write(UORF.get_start_stop_bed(uorf_count))

            total_time_passed = int(round(time.time() * 1000)) - start_time
            time_per_row = total_time_passed/float(max(row_nr, 1))
            time_left = time_per_row * (total_transcript_nr - row_nr)
            m, s = divmod(time_left/1000, 60)
            h, m = divmod(m, 60)

    logging.info(
        "Total transcript nr: %s; Invalid transcript nr: %s Total uORF nr: %s" % (
        transcript_nr, invalid_transcript_nr, uorf_count))
    logging.info("end")


# MAIN
if __name__ == "__main__":
    main()
