import logging
import tempfile
import os

from types import *

logger = logging.getLogger(__name__)

def read_fasta_just_get_sequence(filename):
    """
    Only works for fasta files containing a single sequence!!
    Keeps the case (uppercase/lowercase) of the sequence.
    :param filename:
    :return:
    """
    with open(filename, "r") as  fh:
        # save the fasta sequence into a variable
        fasta = fh.read()
    # but skip the first line (fasta header) when returning the sequences
    return "".join(fasta.split()[1:])


def write_fasta(seq, name="seq"):
    """
    Writes a fasta file
    """
    fh = tempfile.NamedTemporaryFile(delete=False, suffix=".fasta")
    fh.writelines(">%s\n%s" % (name, seq))
    fh.close()
    return fh.name


def make_region_coordinates_relative(regions, correction, strand='+'):
    """
    Correct the regions by a number specified in correction
    """
    new = []
    logging.debug("make_region_coordinates_relative: regions: %s \n correction: %s" % (regions, correction))
    for region in regions:
        if strand == '+':
            new.append([region[0], region[1] - correction, region[2] - correction])
        else:
            start = correction - region[1]
            end = correction - region[2]
            new.append([region[0], start, end])
    return new


def get_codon_list(seq, seq_start, seq_end, strand, start_frame=0, start_codons=[]):
    """
    Returns three lists of codons, includes their absolute coordinates
    on the chromosome (one coordinate for each nucleotide)!
    Start_offset is the offset that is applied to the chromosome
    coordinates.
    One list per frame.
    The returned list contains one tuple per codon and looks like this:
    [[('ATA', 3145666, 3145667, 3145668), ...], [('TAT', 3145667, 3145668, 3145669), ...], [...]]
    The elements are instances of the class Codon!
    We are using the same coordinate system as BEDTools, which means that
    the start coordinate is 0-based and the end coordinate is 1-based!
    If specifying  the first base, we write: 0,1
    If the first three bases are meant, write: 0,3
    #This function was tested on 2016-09-19 and works correctly!
    """
    start_offset = 0
    if strand == "+" or strand == "-":
        start_offset = seq_start
    codon_list = []
    s = get_sequence_with_coordinates(seq, seq_start, seq_end, strand)
    if strand == "-":
        # logging.debug("start_offset: %s" % start_offset)
        start_offset -= len(s)
        # logging.debug("start_offset: %s" % start_offset)
    logging.debug("get_codon_list Start offset: %s\tFrame:%s\nSequence: %s" % (start_offset, start_frame, s))
    for f in range(3):
        codon_list.append(get_codons_without_intronic_regions(s, f, start_codons))
    logging.debug("codon_list: %s" % codon_list)
    return codon_list


def get_start_stop_codons(codon_list):
    """
    Takes the 5' UTR sequence, then chops it into codons, for each of the three frames.
    It then generates 2 lists, consisting of three sublists each, for absolute coordinates.
    Each sublist contains the start/stop codon indices, with one sublist per frame.
    """
    start_codons_abs, stop_codons_abs = [[], [], []], [[], [], []]
    # iterate over each frame
    for f, codon_frames in enumerate(codon_list):
        for CODON in codon_frames:
            if CODON.is_start_codon():
                start_codons_abs[f].append(CODON)
            elif CODON.is_stop_codon():
                stop_codons_abs[f].append(CODON)
    return start_codons_abs, stop_codons_abs


def get_sequence_with_coordinates(seq, seq_start, seq_end, strand):
    """
    Returns a list of tuples, where each nucleotide in the sequence
    is one tuple consisting of the nucleotide and its position within
    the sequence.
    E.g.: [('A', 143546), ('T', 143547), ('G', 143548), ('C', 143549)]
    """
    if strand == "+":
        return [(n, i + seq_start + 1) for i, n in enumerate(seq) if not n.islower()]
    elif strand == "-":
        return [(n, seq_end - i) for i, n in enumerate(seq) if not n.islower()]


def get_codons_without_intronic_regions(seq, f, start_codons):
    """
    Returns a list consisting of tuples. Each tuple gives the codon sequence, and
    three chromosome positions.
    "seq" needs to be output of get_sequence_with_coordinates
    E.g.: [('ATG', 545640, 545641, 545642), ('CTA', 545643, 545810, 545811), ...]
    """
    from classes.codon import Codon
    codon, positions = "", []
    codon_list = []
    i = 1
    for nuc, pos in seq[f:]:
        codon += nuc
        positions.append(pos)
        if i % 3 == 0 and i != 0:
            CODON = Codon(codon_seq=codon,
                          codon_start_pos=positions[0],
                          codon_mid_pos=positions[1],
                          codon_end_pos=positions[2],
                          start_codons=start_codons)
            codon_list.append(CODON)
            codon, positions = "", []
        i += 1
    return codon_list


def get_mask_regions_minus_strand(txStart, txEnd, seq_start, seq_end, intron_coordinates):
    """
    All positions can be given in absolute coordinates!!
    :param txStart:
    :param txEnd:
    :param seq_start:
    :param seq_end:
    :param intron_coordinates:
    :return:
    """
    new = []
    offset = txEnd - seq_end
    for region in intron_coordinates:
        intron_start = region[1]
        intron_end = region[2]
        new_end = (txEnd - intron_start) - offset
        new_start = (txEnd - intron_end) - offset
        new.append((region[0], new_start, new_end))
    return new


def mask_sequence(seq, mask_regions):
    """
    Returns a masked string of the seq.
    :param seq:
    :param mask_regions:
    :return:
    """
    # logging.debug("unmasked sequence: %s length: %s" % (seq, len(seq)))
    # logging.debug("mask_sequence: mask_regions %s" % mask_regions)
    regions = [region for region in mask_regions if region[1] >= 0]
    # logging.debug("mask_sequence: mask_regions after removing those below zero %s" % regions)
    if not regions:
        # logging.debug("No regions to mask in mask_regions %s" % mask_regions)
        return seq
    # sort the regions by the start of the region
    regions.sort(key=lambda x: x[1])
    # logging.debug("mask regions: %s" % regions)
    seq = seq.upper()
    # logging.debug("Unmasked sequence has length %s" % (len(seq)))
    # add the first part of the sequence, up to the first masked part
    masked_seq = [seq[0:regions[0][1]]]
    # logging.debug("masked_seq: %s" % masked_seq)
    end = None
    for i, region in enumerate(regions):
        start = region[1]
        # logging.debug("%s Before adding exon Region: %s, %s-%s, masked_seq: %s" % (i, region, start, end, masked_seq[-100:]))
        if not i == 0:
            # logging.debug("Adding this seq (%s-%s): %s" % (end, start, seq[end:start]))
            masked_seq.append(seq[end:start])
        end = region[2]
        # logging.debug("%s After adding exon Region: %s, %s-%s, masked_seq: %s" % (i, region, start, end, masked_seq[-100:]))
        # logging.debug(">intron%s\n%s" % (i, seq[start:end].lower()))
        masked_seq.append(seq[start:end].lower())
    # append the last part
    # logging.debug("Final masked sequence of length %s: %s" % (len(masked_seq), masked_seq))
    masked_seq.append(seq[end:])
    return "".join(masked_seq)


def get_intron_nucleotide_count_between_two_positions(start, end, intron_coordinates):
    """
    The start and end coordinate are NOT allowed to lie within any intron!
    Start and end are supposed to be chromosome coordinates.
    """
    if start > end:
        start, end = end, start
    nucleotide_count = 0
    logging.debug("start: %s end: %s intron_coordinates: %s" % (start, end, intron_coordinates))
    for intron in intron_coordinates:
        intron_start = intron[1]
        intron_end = intron[2]
        # intron lies inside region
        if intron_start > start and intron_end < end:
            nucleotide_count += intron_end - intron_start
        elif intron_start > end:
            continue
        elif intron_start > start and intron_end > end:
            logging.debug("get_intron_nucleotide_count_between_two_positions: The region ends within the intron!")
            logging.debug(
                "get_intron_nucleotide_count_between_two_positions: start %s end %s, intron_coordinates: %s" % (
                    start, end, intron_coordinates))
    logging.debug("Within the range %s-%s there are %s intron nucleotides." % (start, end, nucleotide_count))
    return nucleotide_count


def position_is_at_intronic_region(pos, intron_coordinates):
    """
    Returns True if the position is within an intronic region,
    and False if not.
    """
    for intron in intron_coordinates:
        start = intron[1]
        end = intron[2]
        if pos > start and pos <= end:
            logging.debug("intron: %s      %s >= %s and %s <= %s" % (intron, pos, start, pos, end))
            return True
    return False


def reverse_complement_seq(seq):
    """
    Returns the reverse complement of a sequence.
    """
    from Bio.Seq import Seq
    SEQ = Seq(seq)
    SEQ = SEQ.reverse_complement()
    return str(SEQ)


def get_exon_nucleotides_ahead_of_pos(pos, number_ahead, intron_coordinates, chrom, fasta_genome_path):
    seq = ""
    ahead = 0
    ahead_pos = pos
    logger.debug("ahead_pos: %s" % pos)

    while ahead < number_ahead:
        if not position_is_at_intronic_region(ahead_pos, intron_coordinates):
            logger.debug("position_is_at_intronic_region: False for %s" % ahead_pos)
            ahead += 1
            part_seq = get_dna_seq(chrom, ahead_pos, ahead_pos + 1, fasta_genome_path)
            if part_seq == "":
                # in case the region is outside the contig
                seq += "X"
            else:
                seq += part_seq[0]
        else:
            logger.debug("position_is_at_intronic_region: True for %s" % ahead_pos)
        ahead_pos -= 1
    logger.debug("get_exon_nucleotides_ahead_of_pos seq: %s" % seq)
    return seq[::-1]


def get_exon_nucleotides_behind_pos(pos, number_behind, intron_coordinates, chrom, fasta_genome_path):
    seq = ""
    behind = 0
    behind_pos = pos + 1
    while behind < number_behind:
        if not position_is_at_intronic_region(behind_pos, intron_coordinates):
            logger.debug("position_is_at_intronic_region: False for %s" % behind_pos)
            behind += 1
            part_seq = get_dna_seq(chrom, behind_pos, behind_pos + 1, fasta_genome_path)
            if part_seq == "":
                # in case the region is outside the contig
                seq += "X"
            else:
                seq += part_seq[0]
        behind_pos += 1
    logger.debug("get_exon_nucleotides_behind_pos seq: %s" % seq)
    return seq


def get_seq_without_introns(seq):
    """
    Returns a sequence without masked regions, i.e. replacing
    all the lower case letters.
    :param seq:
    :return:
    """
    return seq.replace('a', '').replace('c', '').replace('t', '').replace('g', '').replace('n', '')


def get_length_of_introns(seq):
    """
    Returns a sequence without masked regions, i.e. replacing
    all the lower case letters.
    :param seq:
    :return:
    """
    c = 0
    for n in ['a', 'c', 't', 'g']:
        c += seq.count(n)
    return c


def longblob2list(longblob):
    """
    Converts a longblob variable into a list of integers.
    "90930917,90931703,90932054," > [90930917,90931703,90932054]
    """
    if not isinstance(longblob, str):
        print("ERROR: wrong type: %s %s" % (longblob, type(longblob)))
    if not longblob:
        return []
    return [int(number) for number in longblob.split(',') if number != "" and number != ","]


def get_dna_seq(chrom, start, end, fasta_genome_path):
    """
    Returns the dna sequence for a specific chromosome stretch.
    Takes absolute chromosome coordinates!
    """
    from tempfile import NamedTemporaryFile
    if start < 8000: logging.warning("get_dna_seq: Start coordinate %s doesn't look like a valid chromosome "
                                     "position!" % (start))
    logging.debug("get_dna_seq: start: %s end: %s" % (start, end))

    if start == end:
        return ""
    fh = NamedTemporaryFile(delete=False, suffix=".fasta")
    cmd = """samtools faidx "%s" %s:%s-%s > "%s" """ % (fasta_genome_path, chrom, start, end, fh.name)
    logging.debug("get_dna_seq: %s" % cmd)
    os.system(cmd)
    seq = read_fasta_just_get_sequence(fh.name).upper()
    # logging.debug("get_dna_seq: seq: %s" % seq)
    os.remove(fh.name)
    return seq


def get_masked_seq(chrom, start, end, strand, fasta_genome_path, intron_coordinates):
    """
    Returns a specific sequence, i.e. CDS, 3'UTR or full gene sequence
    with all introns masked as lower case.

    E.g. for CDS start:
    start = self.ucsc['cdsStart'] + 1
    end = self.ucsc['cdsEnd']

    Note the +1 !!! This needs already be added when calling
    get_masked_seq!
    """
    logging.debug("Get seq: start: %s, end: %s" % (start, end))
    seq = get_dna_seq(chrom, start, end, fasta_genome_path)
    logging.debug("get_masked_seq: intron_coordinates: %s\nunmasked seq: %s" % (intron_coordinates, seq))
    masked_seq = mask_sequence(seq, make_region_coordinates_relative(intron_coordinates,
                                                                     start - 1))
    if strand == '-': masked_seq = reverse_complement_seq(masked_seq)
    logging.debug("Masked sequence: %s" % masked_seq)
    return masked_seq


def get_line_count_for_file(filename):
    with open(filename, 'r') as fh:
        return sum(1 for line in fh)
